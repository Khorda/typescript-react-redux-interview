# React Interview

## Run

* Clone the current project in a folder
* `cd` that folder
* Run `yarn install`
* Run `yarn start`
* A browser window will automatically spawn, or navigate to `http://localhost:3000/
`

## Details

### Behaviour
The app implements the Final State Machine described in [Specs](#specs). As a FSM,
the app is based on a shared current state, a
set of Nodes, representing each possible State, and a set of Edges, representing each
possible evolution of the FSM (including conditions required to evolve). 
When an interaction occurs, it has its effects both on internal component state and on
external current application state; internal state is necessary to calculate the conditions
required to make a switch on the overall app state. That's why the only responsible
to verify if a state change should occur or not, is delegated to the part who knows
the application state, and shouldn't do nothing else than a simple test and an update
on the current state. 

### Tests
Only rendering tests are included for each component. Interaction tests have been avoided due to lack of time and
knowledge of the library. Tests are made using [react-testing-library], as suggested. 
This should not be the only evaluation of a 'Testing skill score', because a good knowledge
of this practice should be evaluated on known patterns and how / when to use them, from unit to e2e testing.

### UI
Interface is closest to the mockups supplied. Small adjustments would have be done if necessary.
CSS (and obviously, HTML) together with Bootstrap, are mainly used to shape the interface. 

## Specs

The app starts in A.

When the app starts, it makes a request for the maximum that the counter can reach.
The request is made using the `getNumber` function in the `skd.ts` file.
The `getNumber` function returns a promise.

If the request fails, the app must go to D.

The `60 seconds` string is a countdown.
The countdown always starts from 60 seconds.

Clicking on `+` increases the counter (second row, in the middle), clicking on `-` decreases it.
The counter must never go above the maximum (4 in the examples) or below zero.

In B1/C1, clicking on `Show QR code` displays the QR code.

In B2/C2, clicking on `Hide QR code` hides the QR code.

The QR code must display the value `chainsidepay`.

Clicking on `+` in B1 takes the app to C1.
Clicking on `+` in B2 takes the app to C2.
Same goes when clicking on `-` enough times from C1/C2.

Clicking on `Cancel` on the top right in B1/B2 takes the app to E.

When the counter reaches the maximum, the app goes to F.

When the timer expires, the app goes to G.

In E and G, clicking on `Reset` refreshes the page.

In F, clicking on `Continue` takes you to `http://example.com`.

## Implementation Requirements

Implement the app in React, using TypeScript and Redux.

Unit test components and functions.
To test the components, use [react-testing-library].

[react-testing-library]: https://github.com/kentcdodds/react-testing-library
