import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {createStore} from 'redux';
import {stepNext} from './reducers';
import {StoreState} from './types';
import {A} from "./constants";
import {SetNodeAction} from "./actions";
import {Provider} from "react-redux";

const store = createStore<StoreState, SetNodeAction, any, any>(stepNext, {
    actualNode: A
});

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();
